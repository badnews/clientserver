# C network client/server exercise #
-------------------------------------

Ravi Chandra / ravi@chandra.cc / 4 April 2013

## Objective ##

Write a C network server to send a file to a client. Write a client consumer. The server should be able to service multiple clients concurrently.

***Optional*** Assume that the link is slow and expensive. If the client already has the file but it differs from the server copy, the client should be able to update his copy in a manner that makes efficient and economical use of the network resources.

## Implementation notes ##

The C server is, relatively, straight-forward. Common functionality is in the `common.c` module.

#### Efficient delta transfers ####

To facilitate the (fun) optional requirement I have added a mechanism by which the client can detect differences within pieces of a file and then only request those pieces that have actually changed.

This is implemented by logically splitting a file into `PIECE_SIZE`-bytes parts and then using the **SHA1** algorithm to calcuate a signature (hash) for each piece. The client can request a list of hash pieces from the server and then verify the signatures with its own copy. Any piece which is different can be requested from the server. The server uses `sendfile()` with an *offset* and *count* value to send the correct file piece instead of the whole thing.

#### Concurreny ####

I have chosen to use threads to handle concurrent clients. In this instance, threads work fine without too much hassle because the server does not modify any data. Therefore, no mutexes or semaphores and other fun stuff are required. It also keeps the code almost identical to a non-current server.

Forking could also work, and is argubaly easier than threads when dealing with a server that has to write. However it is generally more heavyweight.

Another option is non-blocking I/O multiplexing, for example `select()` or, practically, `epoll()` or `kqueue()` (or an abstraction layer to use the appropriate mechanism depending on platform.

#### mmap() vs read() ####

In the server I have used `mmap()` to map the file into virtual address space. In a realistic scenario this may not be the best approach, would probably need to thinking about the file sizes involved and the other operations we needed to do on the file.

## Run ##

The example code can be run using the bash build script as follows:

1) build program(s). Binaries will be stored in `bin/`

`$ make clean && make`

2) run a server (sequential or concurrent, respectively). Make sure you have ***file.txt*** in your path!:

`$ bin/server` or `$ bin/thr_server`

3) launch a client in an additional terminal:

`$ bin/client`

Client output as follows:

    $ bin/client 
    ..
    FILE HASH MATCHED!
    - piece hash list -
    169812b76ae14204f0e9ae8750ab4ef3f84d5612
    0a0ef45de19304c4700b2a953daff60cb324f008
    4f78ce35ba4cc7e908b86343d74e73a362b58e8c
    b976f1abba599f4e25c4a6d05bac7b0aa1cd1824
    4c0b73d586a06cf00c925aac19e654e2460f51d1
    ..
    TAMPERING WITH FILE DATA
    TAMPERED WITH BYTE 1834
    TAMPERED WITH BYTE 785
    TAMPERED WITH BYTE 3165
    TAMPERED WITH BYTE 520
    ..
    FILE HASH NOT-MATCHED! EXPECTED, WE JUST MODIFIED THE DATA.
    - piece hash list -
    9a2508c43596ff28e97133d288032f05eb406408
    810dfd42225a4cd5adcd6bea980194feee42909b
    4f78ce35ba4cc7e908b86343d74e73a362b58e8c
    878255450f6f3186e0eda0941ed0a51cf58b3654
    4c0b73d586a06cf00c925aac19e654e2460f51d1
    ..
    PIECE 0: ERROR!
    PIECE 1: ERROR!
    PIECE 2: OK
    PIECE 3: ERROR!
    PIECE 4: OK
    ..
    GETTING ERRONEOUS PIECES ONLY
    ..
    RECOMPUTING DATA MATCH
    PIECE 0: OK
    PIECE 1: OK
    PIECE 2: OK
    PIECE 3: OK
    PIECE 4: OK
    FILE HASH MATCHED!
    ..
    FINISHED


#### Platform notes ####

All programs have been verified on OS X 10.8.2 (gcc-4.2) and Ubuntu Linux 12.04 x86_64 (gcc-4.6).
