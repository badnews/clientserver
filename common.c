//
// common functions to client and server
// mainly convenience methods for socket and hash functionals
//   ravi@chandra.cc 4/4/13
//

#include "common.h"
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


// convenience functions for printing hashes
void print_sha_digest(uint8_t* shasum) {
    uint8_t i;
    for (i=0; i<SHA_DIGEST_LENGTH; i++) {
        printf("%02x", shasum[i]);
    }
    printf("\n");
}

void print_piece_hash_list(piece_hash_s* root, int num_pieces) {
    uint8_t i;
    piece_hash_s *p = root;
    printf("- piece hash list -\n");
    for (i=0; i<num_pieces; i++) {
        print_sha_digest(p->hash);
        p = p->next;
    }
    printf("\n");
}

void bytes_to_hex_str(uint8_t *src, uint8_t *dest, size_t len) {
    static size_t k;
    uint8_t *q = src;
    uint8_t *p = dest;
    for (k=0; k<len; k++) {
        sprintf(p, "%02x", *q++);
        p += 2;
    }
}

void hex_str_to_bytes(uint8_t *src, uint8_t *dest, size_t len) {
    size_t i;
    int8_t *p = (int8_t *)src;
    uint8_t *q = dest;
    for (i=0; i<len; i++) {
        sscanf(p, "%02x", q++);
        p += 2;
    }
}

// wrappers for sending and receiving our "command strings"
void recv_string(uint8_t *buffer, size_t len, int sock_fd) {
    size_t rl, delim;
    buffer[0] = '\0';  // destroy any existing message
    rl = recv(sock_fd, buffer, len, 0);
    printf("RECEIVED %d BYTES\n", (int)rl);
    // find \r\n delimeter and terminate string! (e.g. from telnet session)
    delim = strcspn(buffer, "\r\n");
    buffer[delim] = '\0';
    printf("RECEIVED STRING: %s\n", buffer);
}

void send_string(const int8_t *msg, int sock_fd) {
    static int8_t line[50];
    // append a trailing \r\n to match receive
    sprintf(line, "%s\r\n", msg);
    send(sock_fd, line, strlen(line), 0);
}

void compute_piece_hashes(file_info_s *file_info, uint8_t *addr) {
    int8_t i;
    piece_hash_s *root, *piece;

    root = malloc(sizeof(piece_hash_s));
    file_info->piece_hash_list = root;
    SHA1(addr, PIECE_SIZE, root->hash);
    for (i=1; i<file_info->num_pieces; i++) {
        piece = malloc(sizeof(piece_hash_s));
        root->next = piece;
        piece->next = NULL;
        root = piece;
        if (i<file_info->num_pieces-1)
            SHA1(addr+(i*PIECE_SIZE), PIECE_SIZE, piece->hash);
        else
            // calculate different data size for the last piece
            SHA1(addr+(i*PIECE_SIZE), file_info->size-(i*PIECE_SIZE), piece->hash);
    }
}
