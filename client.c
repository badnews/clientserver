//
// client module to demonstrate functionality
//   ravi@chandra.cc 4/4/13
//

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <openssl/sha.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "common.h"

// number of bytes to modify
#define TAMPER_COUNT 4

// global buffer for file data, 16KiB is enough for testing
uint8_t file_buff[16536];

// we'll only have one file so let's make it a global
file_info_s file_info;

// TODO: refactor and with get_erroneous_pieces()
size_t verify_erroneous_pieces(int num_pieces, piece_hash_s* a, piece_hash_s* b,
                               uint8_t *error_list) {
    int8_t i;
    uint8_t *err = error_list;
    size_t err_count = 0;
    for (i=0; i<num_pieces; i++) {
        printf("PIECE %d: ", i);
        if (memcmp(a->hash, b->hash, sizeof(a->hash)) == 0) printf("OK\n");
        else {
            *err++ = i;
            err_count++;
            printf("ERROR!\n");
        }
        a = a->next;
        b = b->next;
    }
    return err_count;
}

void get_erroneous_pieces(int num_pieces, piece_hash_s* a, piece_hash_s* b) {
    int8_t i;
    for (i=0; i<num_pieces; i++) {
        printf("PIECE %d: ", i);
        if (memcmp(a->hash, b->hash, sizeof(a->hash)) == 0) printf("OK\n");
        else printf("ERROR!\n");
        a = a->next;
        b = b->next;
    }
}

piece_hash_s *get_hash_list(int sock_fd, int num_pieces) {
    size_t rl;
    int8_t i;
    piece_hash_s *root, *piece, *new;
    static uint8_t sha_buff[SHA_DIGEST_LENGTH*2+2]; // receive buffer

    send_string(CMD_HASH_LIST, sock_fd);

    // get first hash in list
    recv_string(sha_buff, sizeof(sha_buff), sock_fd);
    // make and calculate piece hash
    new = root = malloc(sizeof(piece_hash_s));
    hex_str_to_bytes(sha_buff, new->hash, sizeof(new->hash));
    // loop over remaining pieces
    for (i=1; i<num_pieces; i++) {
        piece = malloc(sizeof(piece_hash_s));
        new->next = piece;
        piece->next = NULL;
        new = piece;
        recv_string(sha_buff, sizeof(sha_buff), sock_fd);
        hex_str_to_bytes(sha_buff, new->hash, sizeof(new->hash));
    }
    return root;
}

size_t get_specific_piece(int sock_fd, int piece_num) {
    int8_t piece_str[10];
    size_t rl;
    send_string(CMD_PIECE, sock_fd);
    sprintf(piece_str, "%d", piece_num);
    send_string((const int8_t*)&piece_str, sock_fd);
    rl = recv(sock_fd, file_buff+(piece_num*PIECE_SIZE), PIECE_SIZE, 0);
    printf("RECEIVED %d BYTES\n", (int)rl);
    return rl;
}

size_t get_entire_file(int sock_fd) {
    size_t rl;
    send_string(CMD_FILE, sock_fd);
    rl = recv(sock_fd, file_buff, sizeof(file_buff), 0);
    printf("RECEIVED %d BYTES\n", (int)rl);
    return rl;
}

// automated test function to show what a potential client could do
void test_process(int sock_fd) {
    uint8_t msg_buff[100];
    uint8_t recv_hash[SHA_DIGEST_LENGTH];

    // wait for hello message
    recv_string(msg_buff, sizeof(msg_buff), sock_fd);
    assert(strcmp(CMD_HELLO, (char *)msg_buff) == 0);
    
    // get the file hash
    send_string(CMD_HASH, sock_fd);
    recv_string(msg_buff, sizeof(msg_buff), sock_fd);
    // convert to byte array for later comparison
    hex_str_to_bytes(msg_buff, recv_hash, sizeof(recv_hash));

    // download the entire file into the global buffer
    file_info.size = get_entire_file(sock_fd);
    file_info.num_pieces = file_info.size/PIECE_SIZE+1,

    // calculate full file hash
    SHA1(file_buff, file_info.size, file_info.hash);
    assert(memcmp(recv_hash, file_info.hash, sizeof(file_info.hash)) == 0);
    printf("FILE HASH MATCHED!\n");

    // make piece hashes
    compute_piece_hashes(&file_info, (uint8_t *)&file_buff);
    print_piece_hash_list(file_info.piece_hash_list, file_info.num_pieces);

    // root of received hash list
    piece_hash_s *root = get_hash_list(sock_fd, file_info.num_pieces);
    print_piece_hash_list(root, file_info.num_pieces);
    
    // randomly tamper with the in-memory file buffer
    printf("\nTAMPERING WITH FILE DATA\n");
    // NOTE: DEFAULT SEED for testing!
    srand(10000);
    uint8_t i;
    uint16_t loc;
    for (i=0; i<TAMPER_COUNT; i++) {
        do {
            loc = rand();
        } while (loc >= file_info.size);
        file_buff[loc] = '\0';
        printf("TAMPERED WITH BYTE %d\n", loc);
    }

    // calculate full file hash
    SHA1(file_buff, file_info.size, file_info.hash);
    assert(memcmp(recv_hash, file_info.hash, sizeof(file_info.hash)) != 0);
    printf("\nFILE HASH NOT-MATCHED! EXPECTED, WE JUST MODIFIED THE DATA.\n");
    
    // recompute new (tampered) piece hashes
    compute_piece_hashes(&file_info, (uint8_t *)&file_buff);
    print_piece_hash_list(file_info.piece_hash_list, file_info.num_pieces);

    // print the pieces with problems
    uint8_t error_list[100];
    size_t num_errs;
    num_errs = verify_erroneous_pieces(file_info.num_pieces, file_info.piece_hash_list,
                                       root, (uint8_t *)&error_list);

    // download only the bad pieces
    printf("\nGETTING ERRONEOUS PIECES ONLY\n");
    for (i=0; i<num_errs; i++) get_specific_piece(sock_fd, error_list[i]);

    // recompute and verify
    printf("\nRECOMPUTING DATA MATCH\n");
    compute_piece_hashes(&file_info, (uint8_t *)&file_buff);
    num_errs = verify_erroneous_pieces(file_info.num_pieces, file_info.piece_hash_list,
                                       root, (uint8_t *)&error_list);
    
    // calculate full file hash
    SHA1(file_buff, file_info.size, file_info.hash);
    assert(memcmp(recv_hash, file_info.hash, sizeof(file_info.hash)) == 0);
    printf("FILE HASH MATCHED!\n");
    
    printf("\nFINISHED\n");
    close(sock_fd);
}

int make_connection() {
    int32_t sock_fd;
    size_t rl;
    
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(LISTEN_PORT),
        .sin_addr.s_addr = htonl(INADDR_LOOPBACK),
    };
    bzero(&addr.sin_zero, sizeof(addr.sin_zero));
    
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    assert(sock_fd >= 0);
    
    connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr));
    
    return sock_fd;
}

int main(int argc, char** argv) {
    test_process(make_connection());
    return (EXIT_SUCCESS);
}