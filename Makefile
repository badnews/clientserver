CC=gcc
CFLAGS=-O2 
DIRS=bin build

all: dirs bin/server bin/thr_server bin/client

dirs:
	mkdir -p $(DIRS)
	cp file.txt bin/

.PHONY: dirs

bin/client: build/common.o build/client.o
	$(CC) build/common.o build/client.o -o bin/client -lcrypto

bin/server: build/common.o build/server.o
	$(CC) build/common.o build/server.o -o bin/server -lcrypto

bin/thr_server: build/common.o build/thr_server.o
	$(CC) build/common.o build/thr_server.o -o bin/thr_server -lcrypto -lpthread

build/common.o:
	$(CC) $(CFLAGS) -c common.c -o build/common.o

build/client.o:
	$(CC) $(CFLAGS) -c client.c -o build/client.o

build/server.o:
	$(CC) $(CFLAGS) -c server.c -o build/server.o

build/thr_server.o:
	$(CC) $(CFLAGS) -c thr_server.c -o build/thr_server.o

clean:
	rm -rf $(DIRS)
