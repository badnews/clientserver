#ifndef _COMMON_H
#define _COMMON_H

#include <inttypes.h>
#include <openssl/sha.h>

#define min(m,n) ((m) < (n) ? (m) : (n))
#define max(m,n) ((m) > (n) ? (m) : (n))


// piece size in bytes
#define PIECE_SIZE 1024
// TCP listen port
#define LISTEN_PORT 3330

// commands
#define CMD_HELLO "HELLO"
#define CMD_HASH "HASH"
#define CMD_HASH_LIST "HASH_LIST"
#define CMD_FILE "FILE"
#define CMD_PIECE "PIECE"
#define CMD_STOP "STOP"

// linked list of hashes
typedef struct piece_hash {
    uint8_t hash[SHA_DIGEST_LENGTH];
    struct piece_hash *next;
} piece_hash_s;

// encapsulate some information about the file of interest
typedef struct file_info {
    size_t size;
    uint8_t hash[SHA_DIGEST_LENGTH];
    uint32_t num_pieces;
    piece_hash_s *piece_hash_list;
    int fd;
} file_info_s;

// common functions
void print_sha_digest(uint8_t* shasum);
void print_piece_hash_list(piece_hash_s* root, int num_pieces);
void send_string(const int8_t *msg, int sock_fd);
void recv_string(uint8_t *buffer, size_t len, int sock_fd);
void bytes_to_hex_str(uint8_t *src, uint8_t *dest, size_t len);
void hex_str_to_bytes(uint8_t *src, uint8_t *dest, size_t len);
void compute_piece_hashes(file_info_s *file_info, uint8_t *addr);

#endif