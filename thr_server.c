//
// server module, supporting threaded concurrency
//   ravi@chandra.cc 4/4/13
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <assert.h>
#include <unistd.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#ifdef __linux__
#include <sys/sendfile.h>
#endif
#include "common.h"
#include <pthread.h>

// we wouldn't normally do this, but this will simplify invocation of the test
#define FILENAME "file.txt"

// we'll only have one file so let's make it a global
file_info_s file_info;

uint8_t ACTIVE = 1;


// wrapper for sendfile to handle pieces and OS portability
void my_sendfile(file_info_s *file_info, int sock_fd, int piece_num) {
    size_t rv;
    off_t offset = piece_num < 0 ? 0 : piece_num*PIECE_SIZE;
#ifndef __linux__
    // for BSD, e.g. OS X!
    off_t len = piece_num < 0 ? (size_t)file_info->size : min(PIECE_SIZE, file_info->size-offset);
    rv = sendfile(file_info->fd, sock_fd, offset, &len, NULL, 0);
    assert(rv == 0);
    printf("--> SEND %d BYTES\n", len);
#else
    size_t count = piece_num < 0 ? (size_t)file_info->size : min(PIECE_SIZE, file_info->size-offset);

    rv = sendfile(sock_fd, file_info->fd, &offset, count);
    if (rv <= 0) {
        perror(NULL);
        printf("errno: %d\n", errno);
    } else {
        printf("--> SEND %u BYTES\n", rv);
    }
#endif
    lseek(file_info->fd, 0, SEEK_SET);
}

void *process_connection(void *arg) {
    int sock_fd = (int)arg;
    int piece_num;
    static uint8_t buffer[100]; // receive buffer
    char sha1hash[SHA_DIGEST_LENGTH*2+1];
    sha1hash[SHA_DIGEST_LENGTH*2] = '\0';

    printf(">> thread %d <<\n", pthread_self());
    printf("PROCESSING\n");
    send_string(CMD_HELLO, sock_fd);
    while (1) {
        recv_string(buffer, sizeof(buffer), sock_fd);

        // do command processing
        if (strcmp(CMD_HASH, (char *)buffer) == 0) {
            // send hash of complete file
            bytes_to_hex_str(file_info.hash, (uint8_t *)sha1hash, SHA_DIGEST_LENGTH);
            send_string(sha1hash, sock_fd);
        } else if (strcmp(CMD_HASH_LIST, (char *)buffer) == 0) {
            // send list of hashes for each piece piece
            int j;
            piece_hash_s *p = file_info.piece_hash_list;
            for (j=0; j<file_info.num_pieces; j++) {
                bytes_to_hex_str(p->hash, (uint8_t *)sha1hash, SHA_DIGEST_LENGTH);
                send_string(sha1hash, sock_fd);
                p = p->next;
            }
        } else if (strcmp(CMD_FILE, (char *)buffer) == 0) {
            // send the entire file contents
            my_sendfile(&file_info, sock_fd, -1);
        } else if (strcmp(CMD_PIECE, (char *)buffer) == 0) {
            // send a specific file piece
            recv_string(buffer, sizeof(buffer), sock_fd);
            piece_num = atoi((const char *)&buffer);
            printf("REQUESTED PIECE: %d\n", piece_num);
            assert(piece_num < file_info.num_pieces);
            my_sendfile(&file_info, sock_fd, piece_num);
        } else if (strcmp(CMD_STOP, (char *)buffer) == 0) {
            // kill the server, UNUSED!
            ACTIVE = 0;
            break;
        } else {
            // close connection, we can't handle anything else
            break;
        }
    }
    close(sock_fd);
    printf("DISCONNECTED\n");
    pthread_exit(NULL);
}

void main_loop(void) {
    int32_t rval, sock_fd, client_fd;
    pthread_t new_thread;

    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(LISTEN_PORT),
        .sin_addr.s_addr = htonl(INADDR_LOOPBACK),
    };
    bzero(&addr.sin_zero, sizeof(addr.sin_zero));
    
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    assert(sock_fd >= 0);
    
    bind(sock_fd, (struct sockaddr *)&addr, sizeof(addr));
    listen(sock_fd, SOMAXCONN);
    
    // main server loop
    do {
        printf("WAITING\n");
        client_fd = accept(sock_fd, NULL, 0);
        printf("CONNECTED\n");
        rval = pthread_create(&new_thread, NULL, process_connection, (void *)client_fd);
        printf("MAIN CONTINUE\n");
    } while (ACTIVE);
}


int main(int argc, char** argv) {
    uint32_t i, *data_ptr;
    int fd;
    struct stat statbuf;
    piece_hash_s *piece, *root;
    void *addr;

    // pre-compute information about the file of interest
    fd = open("file.txt", O_RDONLY);
    fstat(fd, &statbuf);
    file_info.fd = fd;
    file_info.size = statbuf.st_size;
    // mmap() the file into virtual address space.. for a small file it may pay to
    // just read() into a buffer instead!
    addr = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0),
    file_info.num_pieces = statbuf.st_size/PIECE_SIZE+1,

    // calculate full file hash
    SHA1(addr, file_info.size, file_info.hash);
    // calculate piece hashes
    compute_piece_hashes(&file_info, addr);

    print_piece_hash_list(file_info.piece_hash_list, file_info.num_pieces);

    // start the server loop
    main_loop();
    
    return (EXIT_SUCCESS);
}
